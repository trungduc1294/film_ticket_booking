import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:movie_booking/const/seat_name.dart';
import 'package:movie_booking/view/common_widgets/primary_button.dart';
import 'package:movie_booking/view/screens/select_seat/parts/seats.dart';
import 'package:movie_booking/view/screens/select_seat/parts/select_date_time.dart';
import 'package:movie_booking/view/screens/select_seat/select_seat_controller.dart';
import 'package:movie_booking/view/screens/select_seat/widgets/seat_item.dart';
import 'package:movie_booking/view/screens/select_seat/widgets/seat_label.dart';
import 'package:velocity_x/velocity_x.dart';

class SelectSeat extends StatelessWidget {
  SelectSeat({super.key});

  final selectSeatCtrl = Get.find<SelectSeatController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Select Seat"),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              // Screen
              20.heightBox,
              Image(
                image: AssetImage("assets/images/TV_frame.png"),
                fit: BoxFit.fitWidth,
              ),
              20.heightBox,
              Seats(),
              // Select date and time
              38.heightBox,
              "Select Data & Time"
                  .text
                  .size(20)
                  .semiBold
                  .color(AppColors.whiteTextColor)
                  .make(),
              24.heightBox,
              SelectDateTime(),
              // Count total
              40.heightBox,
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Row(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        "Total".text.size(16).color(AppColors.whiteTextColor).make(),
                        8.heightBox,
                        "210.000 VND".text.size(24).color(AppColors.primary).bold.make(),
                      ],
                    ),
                    40.widthBox,
                    Expanded(
                      child: PrimaryButton(
                        btnText: "Buy Ticket", 
                        onTapFunction: () {
                          Get.toNamed("/payment");
                        }
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

// ListView(
//                     scrollDirection: Axis.horizontal,
//                     children: [
//                       Container(
//                         padding: EdgeInsets.all(8),
//                         decoration: BoxDecoration(
//                           color: AppColors.darkGreyBackground,
//                           borderRadius: BorderRadius.circular(100),
//                         ),
//                         child: Column(
//                           children: [
//                             Text(
//                               "Dec",
//                               style: TextStyle(
//                                 color: AppColors.whiteTextColor,
//                                 fontSize: 16,
//                               ),
//                             ),
//                             17.heightBox,
//                             Container(
//                               padding: EdgeInsets.all(8),
//                               decoration: BoxDecoration(
//                                 color: AppColors.greyBackground,
//                                 borderRadius: BorderRadius.circular(100),
//                               ),
//                               child: Text(
//                                 "10",
//                                 style: TextStyle(
//                                   color: AppColors.whiteTextColor,
//                                   fontSize: 16,
//                                 ),
//                               ),
//                             )
//                           ],
//                         ),
//                       ),
//                     ],
//                   )