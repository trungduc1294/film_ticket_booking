import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:movie_booking/const/seat_name.dart';
import 'package:movie_booking/view/screens/select_seat/select_seat_controller.dart';
import 'package:movie_booking/view/screens/select_seat/widgets/seat_item.dart';
import 'package:movie_booking/view/screens/select_seat/widgets/seat_label.dart';
import 'package:velocity_x/velocity_x.dart';

class Seats extends StatelessWidget {
  Seats({super.key});

  final selectSeatCtrl = Get.find<SelectSeatController>();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Obx(() => Wrap(
              spacing: 8,
              runSpacing: 8,
              children: List.generate(
                108,
                (index) => GestureDetector(
                  onTap: () {
                    selectSeatCtrl.chooseSeatIndex(index);
                  },
                  child: SeatItem(
                    seatName: SeatName().seatName[index],
                    seatIndex: index,
                    seatType: selectSeatCtrl.chooseSeat.contains(index) ? 2 
                    : 0,
                  ),
                ),
              ),
            )),
        24.heightBox,
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SeatLabel(
                labelText: "Availble",
                labelColor: AppColors.darkGreyBackground),
            SeatLabel(
                labelText: "Reserved", labelColor: AppColors.innerBackground),
            SeatLabel(labelText: "Selected", labelColor: AppColors.primary),
            SeatLabel(
                labelText: "Other Selecting",
                labelColor: AppColors.greyTextColor),
          ],
        ),
      ],
    );
  }
}
