import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:movie_booking/view/screens/select_seat/select_seat_controller.dart';
import 'package:movie_booking/view/screens/select_seat/widgets/time_item.dart';
import 'package:velocity_x/velocity_x.dart';

class SelectDateTime extends StatelessWidget {
  SelectDateTime({super.key});

  final selectSeatCtrl = Get.find<SelectSeatController>();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 100,
          child: ListView.builder(
            controller: selectSeatCtrl.scrollController,
            scrollDirection: Axis.horizontal,
            itemCount: 31,
            itemBuilder: (context, index) {
              return Obx(() => GestureDetector(
                    onTap: () {
                      selectSeatCtrl.onClickDate(index);
                    },
                    child: Container(
                      margin: EdgeInsets.only(right: 8),
                      height: double.infinity,
                      padding: EdgeInsets.only(
                        left: 6,
                        right: 6,
                        top: 14,
                        bottom: 6,
                      ),
                      decoration: BoxDecoration(
                        color: index == selectSeatCtrl.currentDateIndex.value
                            ? AppColors.primary
                            : index <= selectSeatCtrl.currentDateIndex.value - selectSeatCtrl.offsetDate
                            ? AppColors.darkGreyBackground.withOpacity(0.6)
                            : index >= selectSeatCtrl.currentDateIndex.value + selectSeatCtrl.offsetDate
                            ? AppColors.darkGreyBackground.withOpacity(0.6)
                            : AppColors.darkGreyBackground,
                        borderRadius: BorderRadius.circular(100),
                      ),
                      child: Column(
                        children: [
                          "Dec"
                              .text
                              .semiBold
                              .size(16)
                              .color(AppColors.whiteTextColor)
                              .make(),
                          17.heightBox,
                          Container(
                            width: 40,
                            height: 40,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              color: AppColors.greyBackground,
                              borderRadius: BorderRadius.circular(100),
                            ),
                            child: (index + 1)
                                .text
                                .semiBold
                                .size(16)
                                .color(AppColors.whiteTextColor)
                                .make(),
                          )
                        ],
                      ),
                    ),
                  ));
            },
          ),
        ),
        20.heightBox,
        SizedBox(
          height: 30,
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: [
              TimeItem(index: 0, time: "10:45"),
              TimeItem(index: 1, time: "12:45"),
              TimeItem(index: 2, time: "14:45"),
              TimeItem(index: 3, time: "16:45"),
              TimeItem(index: 4, time: "18:45"),
              TimeItem(index: 5, time: "20:45"),
              TimeItem(index: 6, time: "22:45"),
            ],
          ),
        )
      ],
    );
  }
}
