import 'package:flutter/cupertino.dart';
import 'package:movie_booking/const/colors.dart';

class SeatItem extends StatelessWidget {

  String seatName;
  int seatIndex;
  int seatType; // 0 is available, 1 is reserved, 2 is selected, 3 is orther selecting 

  SeatItem({
    required this.seatName,
    required this.seatIndex,
    required this.seatType,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 28,
      height: 28,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: seatType == 0 ? AppColors.darkGreyBackground : seatType == 1 ? AppColors.innerBackground : seatType == 2 ? AppColors.primary : AppColors.greyTextColor,
        borderRadius: BorderRadius.circular(4),
      ),
      child: Text(
        seatName,
        style: TextStyle(
          color: seatType == 0 ? AppColors.whiteTextColor : seatType == 1 ? AppColors.primary : AppColors.blackBackground,
          fontSize: 10,
        ),
      )
    );
  }
}