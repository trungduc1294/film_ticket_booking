import 'package:flutter/cupertino.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:velocity_x/velocity_x.dart';

class SeatLabel extends StatelessWidget {

  String labelText;
  Color labelColor;

  SeatLabel({
    required this.labelText,
    required this.labelColor,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          width: 24,
          height: 24,
          decoration: BoxDecoration(
            color: labelColor,
            borderRadius: BorderRadius.circular(4),
          ),
        ),
        4.widthBox,
        labelText.text.size(12).color(AppColors.whiteTextColor).make(),
      ],
    );
  }
}
