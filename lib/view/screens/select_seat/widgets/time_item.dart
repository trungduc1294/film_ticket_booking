import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:movie_booking/view/screens/select_seat/select_seat_controller.dart';

class TimeItem extends StatelessWidget {
  int index;
  String time;

  TimeItem({
    required this.index,
    required this.time,
  });

  final selectSeatCtrl = Get.find<SelectSeatController>();

  @override
  Widget build(BuildContext context) {
    return Obx(() => GestureDetector(
          onTap: () {
            selectSeatCtrl.changeSelectedTimeIndex(index);
          },
          child: Container(
            margin: EdgeInsets.only(right: 8),
            padding: EdgeInsets.symmetric(horizontal: 12),
            alignment: Alignment.center,
            decoration: BoxDecoration(
              color: index == selectSeatCtrl.selectTimeIndex.value
                  ? AppColors.innerBackground
                  : AppColors.darkGreyBackground,
              borderRadius: BorderRadius.circular(100),
              border: Border.all(
                color: index == selectSeatCtrl.selectTimeIndex.value
                    ? AppColors.primary
                    : AppColors.darkGreyBackground,
                width: 1,
              ),
            ),
            child: Text(
              time,
              style: TextStyle(
                color: AppColors.whiteTextColor,
                fontSize: 14,
              ),
            ),
          ),
        ));
  }
}
