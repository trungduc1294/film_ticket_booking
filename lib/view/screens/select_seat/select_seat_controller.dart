
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class SelectSeatController extends GetxController {
  var currentDateIndex = 3.obs;
  final offsetDate = 3;
  final ScrollController scrollController = ScrollController();

  var selectTimeIndex = 0.obs;
  
  var chooseSeat = <int>[].obs;

  @override
  void onInit() {
    super.onInit();
    scrollController.addListener(onScroll);
  }

  void onScroll() {
    // final double center = scrollController.position.extentBefore +
    //     scrollController.position.extentInside / 2;
    // currentDateIndex.value = (center / 50).round();

    final double center = scrollController.position.pixels + scrollController.position.viewportDimension / 2;
    int newIndex = (center / 62).round();
    if (currentDateIndex.value != newIndex) {
      currentDateIndex.value = newIndex;
    }
    print(currentDateIndex.value);
  }

  void onClickDate(int index) {
    currentDateIndex.value = index;
    print(currentDateIndex.value);
  }

  void changeSelectedTimeIndex(int index) {
    selectTimeIndex.value = index;
    print(selectTimeIndex.value);
  }

  void chooseSeatIndex(int index) {
    if (chooseSeat.contains(index)) {
      chooseSeat.remove(index);
    } else {
      chooseSeat.add(index);
    }
    print(chooseSeat);
  }
}