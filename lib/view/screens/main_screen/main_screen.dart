import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:movie_booking/view/screens/home_page/home_page.dart';
import 'package:movie_booking/view/screens/main_screen/main_screen_controller.dart';
import 'package:movie_booking/view/screens/movies/movies.dart';
import 'package:movie_booking/view/screens/tickets/tickets.dart';

class MainScreen extends StatelessWidget {
  MainScreen({super.key});

  final mainScreenCtrl = Get.find<MainScreenController>();

  final List<Widget> _pages = [
    HomePage(),
    // Statistic(),
    // Wallet(),
    // Profile(),
    Tickets(),
    Movies(),
    Text("Profile", style: TextStyle(color: Colors.white),),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Obx(
          () => _pages[mainScreenCtrl.bottomNavBarIndex.value],
        ),
      ),
      bottomNavigationBar: Obx(
        () => BottomNavigationBar(
          elevation: 0,
          type: BottomNavigationBarType.fixed,
          currentIndex: mainScreenCtrl.bottomNavBarIndex.value,
          onTap: (value) {
            mainScreenCtrl.changeBottomNavBarIndex(value);
          },
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home_rounded),
              label: "Home",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.confirmation_num),
              label: "Ticket",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.movie),
              label: "Movie",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              label: "Profile",
            ),
          ],
        ),
      ),
    );
  }
}