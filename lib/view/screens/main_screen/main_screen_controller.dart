import 'package:get/get.dart';

class MainScreenController extends GetxController {
  var bottomNavBarIndex = 0.obs;

  void changeBottomNavBarIndex(int index) {
    bottomNavBarIndex.value = index;
    print(bottomNavBarIndex.value);
  }
}