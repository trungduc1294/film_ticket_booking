import 'package:flutter/scheduler.dart';
import 'package:get/get.dart';
import 'package:movie_booking/model/ticket.dart';
import 'package:movie_booking/view/screens/tickets/tickets_controller.dart';

class TicketDetailController extends GetxController {

  final ticketsCtrl = Get.find<TicketsController>();
  
  var ticketDetailId = "".obs;
  late Ticket ticketDetail;

  @override
  void onInit() {
    ticketDetailId.value = Get.arguments["ticket_id"];
    ticketDetail = ticketsCtrl.getTicketById(ticketDetailId.value);
    print("Ticket Detail: ${ticketDetail.filmName}");
    super.onInit();
  }
}