import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_dash/flutter_dash.dart';
import 'package:get/get.dart';
import 'package:get/route_manager.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:movie_booking/view/screens/ticket_detail/ticket_detail_controller.dart';
import 'package:movie_booking/view/screens/ticket_detail/widgets/film_detail.dart';
import 'package:movie_booking/view/screens/ticket_detail/widgets/ticket_detail_item.dart';
import 'package:movie_booking/view/screens/ticket_detail/widgets/ticket_info.dart';
import 'package:velocity_x/velocity_x.dart';

class TicketDetail extends StatelessWidget {
  TicketDetail({super.key});

  final ticketDetailCtrl = Get.find<TicketDetailController>();

  var data = Get.arguments;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Ticket Detail'),
      ),
      body: SafeArea(
        child: Container(
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 24),
            child: Stack(
              children: [
                Container(
                  padding: EdgeInsets.all(24),
                  width: MediaQuery.of(context).size.width,
                  height: 700,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Image(
                            image: NetworkImage(
                              data["thumb"],
                            ),
                            width: 120,
                            fit: BoxFit.fitWidth,
                          ),
                          10.widthBox,
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                "${data["film_name"]}"
                                    .text
                                    .overflow(TextOverflow.ellipsis)
                                    .size(20)
                                    .bold
                                    .make(),
                                8.heightBox,
                                FilmDetail(icon: Icons.schedule, text: "${data["duration"]}"),
                                4.heightBox,
                                FilmDetail(
                                    icon: Icons.videocam_outlined,
                                    text: "Action, Adventure, Fantasy"),
                              ],
                            ),
                          ),
                        ],
                      ),

                      // Seat Info
                      32.heightBox,
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          TicketInfo(
                              iconUrl: "assets/icons/ic_seat.png",
                              title: "Section ${ticketDetailCtrl.ticketDetail.cinemaRoom}",
                              subtitle: "Seat ${ticketDetailCtrl.ticketDetail.seats}"),
                          TicketInfo(
                              iconUrl: "assets/icons/ic_calendar.png",
                              title: "${ticketDetailCtrl.ticketDetail.time}",
                              subtitle: "${ticketDetailCtrl.ticketDetail.date}")
                        ],
                      ),
                      24.heightBox,
                      Divider(),
                      24.heightBox,
                      TicketDetailItem(
                          title: "${ticketDetailCtrl.ticketDetail.total} VND",
                          iconUrl: "assets/icons/ic_money.png"),
                      4.heightBox,
                      TicketDetailItem(
                        title: "${ticketDetailCtrl.ticketDetail.cinemaName}",
                        iconUrl: "assets/icons/ic_location.png",
                        subtitle:
                            "${ticketDetailCtrl.ticketDetail.cinemaAddress}",
                      ),
                      4.heightBox,
                      TicketDetailItem(
                        iconUrl: "assets/icons/ic_location.png",
                        subtitle:
                            "Show this QR code to the ticket counter to receive the ticket",
                      ),
                      Spacer(),
                      GestureDetector(
                        onTap: () {
                          print(data);
                        },
                        child: Image(
                          image: AssetImage('assets/icons/bar_code.png'),
                          width: double.infinity,
                        ),
                      ),
                      16.heightBox,
                      "Order ID: 78777483762398".text.size(12).make(),
                    ],
                  ),
                ),

                // hoa tiet
                Positioned(
                  top: 480,
                  left: -20,
                  child: Container(
                    width: 40,
                    height: 40,
                    decoration: BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.circular(40),
                    ),
                  ),
                ),
                Positioned(
                  top: 480,
                  right: -20,
                  child: Container(
                    width: 40,
                    height: 40,
                    decoration: BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.circular(40),
                    ),
                  ),
                ),
                Positioned(
                  top: 500,
                  child: Dash(
                    direction: Axis.horizontal,
                    length: MediaQuery.of(context).size.width - 32,
                    dashLength: 10,
                    dashColor: AppColors.darkGreyBackground,
                  ),
                ),
              ],
            )),
      ),
    );
  }
}
