import 'package:flutter/material.dart';
import 'package:velocity_x/velocity_x.dart';

class TicketInfo extends StatelessWidget {

  String iconUrl;
  String title;
  String subtitle;

  TicketInfo({required this.iconUrl, required this.title, required this.subtitle});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Image(
          image: AssetImage(iconUrl),
          width: 48,
          fit: BoxFit.fitWidth,
        ),
        8.widthBox,
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            title.text.size(16).make(),
            4.heightBox,
            subtitle.text.size(16).make(),
          ],
        )
      ],
    );
  }
}