import 'package:flutter/material.dart';
import 'package:velocity_x/velocity_x.dart';

class FilmDetail extends StatelessWidget {

  IconData icon;
  String text;

  FilmDetail({required this.icon, required this.text});

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Icon(
          icon,
          size: 16,
        ),
        4.widthBox,
        text.text.size(14).overflow(TextOverflow.ellipsis).make(),
      ],
    );
  }
}
