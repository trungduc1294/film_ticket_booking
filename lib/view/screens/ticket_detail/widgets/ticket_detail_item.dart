import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:velocity_x/velocity_x.dart';

class TicketDetailItem extends StatelessWidget {

  String? title;
  String? subtitle;
  String iconUrl;

  TicketDetailItem({this.title,  this.subtitle, required this.iconUrl});

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Image(
          image: AssetImage(iconUrl),
          width: 24,
        ),
        8.widthBox,
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if(title != null && title!.isNotEmpty) title!.text.size(16).make(),
              // subtitle!.text.size(12).maxLines(2).overflow(TextOverflow.ellipsis).color(AppColors.greyTextColor).make(),
              if(subtitle != null && subtitle!.isNotEmpty) subtitle!.text.size(12).maxLines(2).overflow(TextOverflow.ellipsis).color(AppColors.greyTextColor).make(),
            ],
          ),
        )
      ],
    );
  }
}
