import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/bindings_interface.dart';
import 'package:movie_booking/view/screens/ticket_detail/ticket_detail_controller.dart';
import 'package:movie_booking/view/screens/tickets/tickets_controller.dart';

class TicketBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<TicketDetailController>(() => TicketDetailController());
    Get.lazyPut<TicketsController>(() => TicketsController());
  }
}