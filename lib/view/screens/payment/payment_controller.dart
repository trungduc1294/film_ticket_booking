import 'dart:async';

import 'package:get/get.dart';
import 'package:movie_booking/model/payment_method.dart';

class PaymentController extends GetxController {
  @override
  void onInit() {
    startTimer();
    super.onInit();
  }

  bool stopTimer = false;

  List<PaymentMethod> paymentMethods = [
    PaymentMethod(
        id: 0, image: "assets/icons/zalopay.png", paymentMethod: "Zalo Pay"),
    PaymentMethod(id: 1, image: "assets/icons/momo.png", paymentMethod: "Momo"),
    PaymentMethod(id: 2, image: "assets/icons/visa.png", paymentMethod: "Visa"),
    PaymentMethod(
        id: 3,
        image: "assets/icons/shopeepay.png",
        paymentMethod: "Shoppee Pay"),
    PaymentMethod(
        id: 4, image: "assets/icons/atm_card.png", paymentMethod: "ATM Card"),
  ];

  var selectPaymentMethodIndex = 0.obs;

  void selectPaymentMethod(int index) {
    selectPaymentMethodIndex.value = index;
    print(selectPaymentMethodIndex.value);
  }

  late Timer _timer;
  var seconds = 300.obs; // Đếm ngược từ 60 giây

  void startTimer() {
    seconds.value = 300;
    const oneSec = Duration(seconds: 1);
    _timer = Timer.periodic(
      oneSec,
      (Timer timer) {
        if (seconds == 0 || stopTimer) {
          timer.cancel();
        } else {
          seconds.value--;
          print(seconds.value);
        }
      },
    );
  }

  String getFormattedTime(int totalSeconds) {
    int minutes = totalSeconds ~/ 60; // Tính số phút
    int seconds = totalSeconds % 60; // Tính số giây còn lại sau khi chia cho 60

    // Định dạng hiển thị 00:00
    return '${minutes.toString().padLeft(2, '0')}:${seconds.toString().padLeft(2, '0')}';
  }
}
