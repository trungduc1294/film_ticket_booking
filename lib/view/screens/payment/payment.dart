import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:movie_booking/view/common_widgets/film_card_portrait.dart';
import 'package:movie_booking/view/common_widgets/primary_button.dart';
import 'package:movie_booking/view/screens/payment/parts/discount_code.dart';
import 'package:movie_booking/view/screens/payment/payment_controller.dart';
import 'package:movie_booking/view/screens/payment/widgets/film_detail_item.dart';
import 'package:movie_booking/view/screens/payment/widgets/payment_method_item.dart';
import 'package:velocity_x/velocity_x.dart';

class Payment extends StatelessWidget {
  Payment({super.key});

  final paymentMedthodCtrl = Get.find<PaymentController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Payment'),
        ),
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                FilmCardPortrait(
                  ticketId: "1",
                    film_name: "Avenger Infinity War",
                    duration: "2h30'",
                    genre: "Action",
                    address: "Vincom Ocean Park CGV",
                    thumb: "assets/images/thumb_avenger_infinity.png"),
                32.heightBox,
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    "Order ID"
                        .text
                        .size(16)
                        .color(AppColors.whiteTextColor)
                        .make(),
                    "788893959493"
                        .text
                        .size(16)
                        .semiBold
                        .color(AppColors.whiteTextColor)
                        .make(),
                  ],
                ),
                20.heightBox,
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    "Seat".text.size(16).color(AppColors.whiteTextColor).make(),
                    "H7, H8"
                        .text
                        .size(16)
                        .semiBold
                        .color(AppColors.whiteTextColor)
                        .make(),
                  ],
                ),
                // Discount code
                20.heightBox,
                DiscountCode(),
                32.heightBox,
                Divider(),
                //total
                32.heightBox,
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    "Total"
                        .text
                        .size(16)
                        .color(AppColors.whiteTextColor)
                        .make(),
                    "150,000 VND"
                        .text
                        .size(24)
                        .bold
                        .color(AppColors.primary)
                        .make(),
                  ],
                ),
                // payment method
                32.heightBox,
                "Payment Method"
                    .text
                    .size(24)
                    .bold
                    .color(AppColors.whiteTextColor)
                    .make(),
                24.heightBox,
                Column(
                  children: List.generate(
                      paymentMedthodCtrl.paymentMethods.length,
                      (index) => PaymentMethodItem(
                          id: paymentMedthodCtrl.paymentMethods[index].id,
                          title: paymentMedthodCtrl
                              .paymentMethods[index].paymentMethod,
                          image:
                              paymentMedthodCtrl.paymentMethods[index].image)),
                ),
                // timer
                24.heightBox,
                Container(
                  padding: EdgeInsets.all(16),
                  decoration: BoxDecoration(
                    color: AppColors.innerBackground,
                    borderRadius: BorderRadius.circular(12),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      "Complete your payment in"
                          .text
                          .size(16)
                          .color(AppColors.whiteTextColor)
                          .make(),
                      Obx(
                        () =>
                            "${paymentMedthodCtrl.getFormattedTime(paymentMedthodCtrl.seconds.value)}"
                                .text
                                .size(16)
                                .semiBold
                                .color(AppColors.primary)
                                .make(),
                      ),
                    ],
                  ),
                ),
                32.heightBox,
                PrimaryButton(
                  btnText: "Continue",
                  onTapFunction: () {
                    paymentMedthodCtrl.stopTimer = true;
                    Get.offNamed('/ticket_detail');
                  },
                ),
                32.heightBox,
              ],
            ),
          ),
        ));
  }
}
