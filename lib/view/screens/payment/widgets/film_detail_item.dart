import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:velocity_x/velocity_x.dart';

class FilmDetailItem extends StatelessWidget {
  IconData icon;
  String text;

  FilmDetailItem({required this.icon, required this.text});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(
          icon,
          color: AppColors.whiteTextColor,
          size: 20,
        ),
        4.widthBox,
        text.text.size(12).color(AppColors.whiteTextColor).make(),
      ],
    );
  }
}
