import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:movie_booking/view/screens/payment/payment_controller.dart';
import 'package:velocity_x/velocity_x.dart';

class PaymentMethodItem extends StatelessWidget {

  int id;
  String title;
  String image;

  PaymentMethodItem({required this.id ,required this.title, required this.image});

  final paymentMedthodCtrl = Get.find<PaymentController>();

  @override
  Widget build(BuildContext context) {
    return Obx(() => GestureDetector(
      onTap: () {
        paymentMedthodCtrl.selectPaymentMethod(id);
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        margin: EdgeInsets.only(bottom: 16),
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
          color: id == paymentMedthodCtrl.selectPaymentMethodIndex.value ? AppColors.innerBackground : AppColors.darkGreyBackground,
          borderRadius: BorderRadius.circular(12),
          border: Border.all(color: id == paymentMedthodCtrl.selectPaymentMethodIndex.value ? AppColors.primary : AppColors.darkGreyBackground, width: 1),
        ),
        child: Row(
          children: [
            Image(image: AssetImage(image), height: 50,),
            16.widthBox,
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                title.text.size(20).bold.color(AppColors.whiteTextColor).make(),
              ],
            )
          ],
        ),
      ),
    ));
  }
}