import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:velocity_x/velocity_x.dart';

class DiscountCode extends StatelessWidget {
  const DiscountCode({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      width: double.infinity,
      padding: EdgeInsets.only(left: 16),
      decoration: BoxDecoration(
        color: AppColors.darkGreyBackground,
        borderRadius: BorderRadius.circular(8),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(Icons.confirmation_number_outlined,
              color: AppColors.whiteTextColor),
          4.widthBox,
          Expanded(
            child: TextFormField(
              decoration: InputDecoration(
                hintText: 'Enter your code',
                hintStyle: TextStyle(color: AppColors.greyTextColor),
                border: InputBorder.none,
              ),
            ),
          ),
          Container(
            height: double.infinity,
            padding: EdgeInsets.symmetric(horizontal: 20),
            alignment: Alignment.center,
            decoration: BoxDecoration(
              color: AppColors.primary,
              borderRadius: BorderRadius.circular(8),
            ),
            child: Text('Apply',
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500)),
          )
        ],
      ),
    );
  }
}
