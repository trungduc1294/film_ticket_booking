import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:movie_booking/const/colors.dart';

class SignupController extends GetxController {
  TextEditingController phone_number = TextEditingController();

  bool checkPhoneNumber() {
    print(phone_number.text);
    if (phone_number.text.isEmpty) {
      Get.snackbar(
        "Error",
        "Phone number cannot be empty",
        colorText: AppColors.whiteTextColor,
      );
      return false;
    } else if (!phone_number.text.isNum) {
      Get.snackbar(
        "Error",
        "Phone number must be a number",
        colorText: AppColors.whiteTextColor,
      );
      return false;
    } else if (phone_number.text.length < 9) {
      Get.snackbar(
        "Error",
        "Phone number must be at least 9 characters",
        colorText: AppColors.whiteTextColor,
      );
      return false;
    }
    return true;
  }
}
