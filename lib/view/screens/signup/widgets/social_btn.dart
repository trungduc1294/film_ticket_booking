import 'package:flutter/material.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:velocity_x/velocity_x.dart';

class SocialBtn extends StatelessWidget {

  String imageUrl;
  String text;
  Function()? onTapFunction;

  SocialBtn({required this.imageUrl,required this.text, this.onTapFunction});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTapFunction,
      child: Container(
        decoration: BoxDecoration(
          color: AppColors.darkButtonBg,
          borderRadius: BorderRadius.circular(100),
        ),
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(vertical: 16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image(image: AssetImage('assets/icons/$imageUrl')),
            10.widthBox,
            text.text.size(16).bold.color(AppColors.whiteTextColor).make(),
          ],
        ),
      ),
    );
  }
}