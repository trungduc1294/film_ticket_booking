import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:movie_booking/view/common_widgets/custom_text_form_field.dart';
import 'package:movie_booking/view/common_widgets/primary_button.dart';
import 'package:movie_booking/view/screens/signup/signup_controller.dart';
import 'package:movie_booking/view/screens/signup/widgets/social_btn.dart';
import 'package:velocity_x/velocity_x.dart';

class Signup extends StatelessWidget {
  const Signup({super.key});

  @override
  Widget build(BuildContext context) {

    final SignupController signupController = Get.find<SignupController>();

    return Scaffold(
      appBar: AppBar(
        title: Text('Sign Up'),
      ),
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            children: [
              80.heightBox,
              CustomTFF(labelText: 'Phone number', prevIcon: Icon(Icons.phone_outlined), controller: signupController.phone_number,),
              32.heightBox,
              PrimaryButton(
                btnText: "Continue", 
                onTapFunction: () {
                  if (signupController.checkPhoneNumber()) {
                    Get.toNamed("/confirm_otp");
                  }
                }
              ),
              Spacer(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    child: Container(
                      height: 1,
                      color: AppColors.greyTextColor,
                    ),
                  ),
                  10.widthBox,
                  "Or continue with".text.size(16).bold.color(AppColors.whiteTextColor).make(),
                  10.widthBox,
                  Expanded(
                    child: Container(
                      height: 1,
                      color: AppColors.greyTextColor,
                    ),
                  ),
                ],
              ),
              40.heightBox,
              SocialBtn(
                imageUrl: "facebook.png", 
                text: "Facebook", 
                onTapFunction: () {
                  print("Facebook");
                },
              ),
              16.heightBox,
              SocialBtn(
                imageUrl: "google.png", 
                text: "Google", 
                onTapFunction: () {
                  print("Google");
                },
              ),
              40.heightBox,
              "By singin or signup, you agree with our Terms of Service and Privacy Policy".text.size(12).align(TextAlign.center).color(AppColors.greyTextColor).make(),
            ],
          ),
        ),
      ),
    );
  }
}
