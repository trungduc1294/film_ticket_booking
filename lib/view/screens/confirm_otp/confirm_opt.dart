import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:movie_booking/view/common_widgets/primary_button.dart';
import 'package:movie_booking/view/screens/confirm_otp/confirm_otp_controller.dart';
import 'package:movie_booking/view/screens/signup/signup_controller.dart';
import 'package:velocity_x/velocity_x.dart';

class ConfirmOTP extends StatefulWidget {
  const ConfirmOTP({super.key});

  @override
  State<ConfirmOTP> createState() => _ConfirmOTPState();
}

class _ConfirmOTPState extends State<ConfirmOTP> {
  @override
  void initState() {
    super.initState();
    final confirmOtpCtrl = Get.find<ConfirmOtpController>();
    confirmOtpCtrl.startTimer();
  }

  @override
  Widget build(BuildContext context) {
    final confirmOtpCtrl = Get.find<ConfirmOtpController>();
    final signupCtrl = Get.find<SignupController>();

    return Scaffold(
      appBar: AppBar(
        title: Text(''),
      ),
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              56.heightBox,
              "Comfirm OTP code"
                  .text
                  .size(32)
                  .color(AppColors.primary)
                  .bold
                  .make(),
              "You just need to enter the OTP sent to the registered phone number ${signupCtrl.phone_number.text}"
                  .text
                  .size(16)
                  .color(AppColors.greyTextColor)
                  .make(),

              // OTP input
              48.heightBox,
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: List.generate(
                  6,
                  (index) => SizedBox(
                    width: 50,
                    child: TextFormField(
                      controller: confirmOtpCtrl.controllers[index],
                      focusNode: confirmOtpCtrl.focusNodes[index],
                      keyboardType: TextInputType.number,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 18,
                          color: AppColors.whiteTextColor,
                          fontWeight: FontWeight.bold),
                      maxLength: 1,
                      onChanged: (value) {
                        confirmOtpCtrl.handleSubmitted(value, index);
                      },
                      decoration: InputDecoration(
                        counterText: '',
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide:
                              BorderSide(color: AppColors.primary, width: 1),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide:
                              BorderSide(color: AppColors.primary, width: 1),
                        ),
                        filled: true,
                        fillColor: AppColors.innerBackground,
                      ),
                    ),
                  ),
                ),
              ),

              // Timer
              24.heightBox,
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Obx(() => Align(
                        child: Text('00:${confirmOtpCtrl.seconds}',
                            style: TextStyle(
                                fontSize: 20,
                                color: AppColors.whiteTextColor,
                                fontWeight: FontWeight.bold)),
                      )),
                  Obx(() => TextButton(
                      onPressed: () {
                        confirmOtpCtrl.startTimer();
                      },
                      child: Text('Resend OTP',
                          style: TextStyle(
                              color: confirmOtpCtrl.seconds == 0
                                  ? AppColors.primary
                                  : AppColors.greyTextColor,
                              fontSize: 16,
                              fontWeight: FontWeight.bold)))),
                ],
              ),

              // submit button
              Spacer(),
              PrimaryButton(
                btnText: "Continue",
                onTapFunction: () {
                  confirmOtpCtrl.createOtp();
                  Get.toNamed("/enter_username");
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
