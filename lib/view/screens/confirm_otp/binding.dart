import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/bindings_interface.dart';
import 'package:movie_booking/view/screens/confirm_otp/confirm_otp_controller.dart';
import 'package:movie_booking/view/screens/signup/signup_controller.dart';

class ConfirmOtpBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ConfirmOtpController>(() => ConfirmOtpController());
    Get.lazyPut<SignupController>(() => SignupController());
    
  }
}