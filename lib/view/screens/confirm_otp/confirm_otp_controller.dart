import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ConfirmOtpController extends GetxController {
  var focusNodes = List.generate(6, (index) => FocusNode());
  var controllers = List.generate(6, (index) => TextEditingController());

  var otp = ''.obs;

  late Timer _timer;
  var seconds = 60.obs; // Đếm ngược từ 60 giây

  void handleSubmitted(String value, int index) {
    if (value.length == 1 && index < 5) {
      focusNodes[index + 1].requestFocus();
    }
  }

  void createOtp() {
    otp.value = '';
    for (var i = 0; i < controllers.length; i++) {
      otp.value += controllers[i].text;
    }
    print(otp.value);
  }

  void startTimer() {
    seconds.value = 60;
    const oneSec = Duration(seconds: 1);
    _timer = Timer.periodic(
      oneSec,
      (Timer timer) {
        if (seconds == 0) {
          timer.cancel();
        } else {
          seconds.value--;
        }
      },
    );
  }
}
