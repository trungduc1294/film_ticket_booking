import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movie_booking/const/colors.dart';

class HomeSearch extends StatelessWidget {
  const HomeSearch({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: TextFormField(
        style: TextStyle(color: AppColors.whiteTextColor),
        decoration: InputDecoration(
          contentPadding: EdgeInsets.zero,
          hintText: "Search",
          hintStyle: TextStyle(
            color: AppColors.greyTextColor,
            fontSize: 16,
          ),
          prefixIcon: Icon(Icons.search, color: AppColors.whiteTextColor),
          filled: true,
          fillColor: AppColors.darkGreyBackground,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide.none,
          ),
        ),
      ),
    );
  }
}
