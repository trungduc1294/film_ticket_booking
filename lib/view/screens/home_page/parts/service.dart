import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:movie_booking/view/screens/home_page/home_page_controller.dart';
import 'package:movie_booking/view/screens/home_page/widgets/part_title.dart';
import 'package:movie_booking/view/screens/home_page/widgets/service_item.dart';
import 'package:velocity_x/velocity_x.dart';

class HomepageService extends StatelessWidget {
  HomepageService({super.key});

  final homepageCtrl = Get.find<HomePageController>();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        PartTitle(
          title: "Service",
          onTap: () {
            // Get.to(() => NowPlayingScreen());
          },
        ),
        24.heightBox,
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              ServiceItem(
                service: homepageCtrl.listService[0].service,
                image_url: homepageCtrl.listService[0].image,
                onTap: () {
                  // Get.to(() => NowPlayingScreen());
                },
              ),
              ServiceItem(
                service: homepageCtrl.listService[1].service,
                image_url: homepageCtrl.listService[1].image,
                onTap: () {
                  // Get.to(() => NowPlayingScreen());
                },
              ),
              ServiceItem(
                service: homepageCtrl.listService[2].service,
                image_url: homepageCtrl.listService[2].image,
                onTap: () {
                  // Get.to(() => NowPlayingScreen());
                },
              ),
              ServiceItem(
                service: homepageCtrl.listService[3].service,
                image_url: homepageCtrl.listService[3].image,
                onTap: () {
                  // Get.to(() => NowPlayingScreen());
                },
              ),
            ]
          ),
        ),
      ],
    );
  }
}
