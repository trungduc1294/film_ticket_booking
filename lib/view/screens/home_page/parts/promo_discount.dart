import 'package:card_swiper/card_swiper.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:movie_booking/view/common_widgets/seall_widget.dart';
import 'package:movie_booking/view/screens/home_page/home_page_controller.dart';
import 'package:movie_booking/view/screens/home_page/widgets/part_title.dart';
import 'package:velocity_x/velocity_x.dart';

class PromoAndDiscount extends StatelessWidget {
  PromoAndDiscount({super.key});

  final homepageCtrl = Get.find<HomePageController>();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        PartTitle(
          title: "Promo & Discount",
          onTap: () {
            
          },
        ),
        24.heightBox,
        Container(
          height: 200,
          child: Swiper(
            itemCount: homepageCtrl.promoList.length,
            itemWidth: 200,
            itemHeight: 200,
            // layout: SwiperLayout.STACK,
            autoplay: true,
            itemBuilder: (context, index) {
              return Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                    image: AssetImage(homepageCtrl.promoList[index]),
                    fit: BoxFit.fill,
                  ),
                ),
              );
            },
          ),
        ),
      ],
    );
  }
}
