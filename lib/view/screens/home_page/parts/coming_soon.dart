import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:movie_booking/view/common_widgets/seall_widget.dart';
import 'package:movie_booking/view/screens/home_page/home_page_controller.dart';
import 'package:movie_booking/view/screens/home_page/widgets/film_card.dart';
import 'package:movie_booking/view/screens/home_page/widgets/part_title.dart';
import 'package:velocity_x/velocity_x.dart';

class HomeComingSoon extends StatelessWidget {
  HomeComingSoon({super.key});

  final homepageCtrl = Get.find<HomePageController>();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        PartTitle(
          title: "Coming Soon",
          onTap: () {
            
          },
        ),
        24.heightBox,
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: List.generate(
              homepageCtrl.listFilm.length,
              (index) => FilmCard(
                id: homepageCtrl.listFilm[index].id,
                image: homepageCtrl.listFilm[index].image,
                name: homepageCtrl.listFilm[index].title,
                genre: homepageCtrl.listFilm[index].desc, // Change to genre
                time: homepageCtrl.listFilm[index].time,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
