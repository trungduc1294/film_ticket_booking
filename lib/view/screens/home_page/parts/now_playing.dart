import 'package:card_swiper/card_swiper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:movie_booking/view/common_widgets/seall_widget.dart';
import 'package:movie_booking/view/screens/home_page/home_page_controller.dart';
import 'package:movie_booking/view/screens/home_page/widgets/part_title.dart';
import 'package:velocity_x/velocity_x.dart';

class HomeNowPlaying extends StatelessWidget {
  HomeNowPlaying({super.key});

  final homepageCtrl = Get.find<HomePageController>();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            PartTitle(
              title: "Now Playing",
              onTap: () {
                
              },
            ),
            // Swiper
            24.heightBox,
            Container(
              height: 500,
              child: Swiper(
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    height: MediaQuery.of(context).size.height,
                    child: Column(
                      children: [
                        Image(
                          image: NetworkImage(homepageCtrl.listFilm[index].image),
                          height: MediaQuery.of(context).size.height * 0.45,
                          fit: BoxFit.fitHeight,
                        ),
                        10.heightBox,
                        Expanded(
                          child: Container(
                            width: 260,
                            child: Column(
                              children: [
                                // "Avengers - Infinity War".text.size(20).color(AppColors.whiteTextColor).bold.make(),
                                Text(
                                  homepageCtrl.listFilm[index].title,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    color: AppColors.whiteTextColor,
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                Text(
                                  "${homepageCtrl.listFilm[index].duration} | ${homepageCtrl.listFilm[index].desc}", // change to genre
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    color: AppColors.greyTextColor,
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(Icons.star,
                                        color: AppColors.primary, size: 10),
                                    4.widthBox,
                                    "${homepageCtrl.listFilm[index].rate}"
                                        .text
                                        .size(10)
                                        .bold
                                        .color(AppColors.whiteTextColor)
                                        .overflow(TextOverflow.ellipsis)
                                        .make(),
                                    "(${homepageCtrl.listFilm[index].popular})"
                                        .text
                                        .size(10)
                                        .color(AppColors.greyTextColor)
                                        .make(),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                },
                itemCount: homepageCtrl.listFilm.length,
                viewportFraction: 0.68,
                scale: 0.8,
                autoplay: true,
                onIndexChanged: (value) {
                  homepageCtrl.onSwiperIndexChanged(value);
                },
              ),
            ),
            // swiper pagination
            16.heightBox,
            Obx(() => Center(
                  child: Container(
                    constraints: BoxConstraints(maxWidth: 16 * homepageCtrl.listFilm.length+1),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      color: AppColors.greyTextColor,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: List.generate(
                        homepageCtrl.listFilm.length,
                        (index) => Container(
                          width: 16,
                          height: 8,
                          decoration: BoxDecoration(
                            color: homepageCtrl.swiperIndex.value == index
                                ? AppColors.primary
                                : AppColors.greyTextColor,
                            borderRadius: BorderRadius.circular(4),
                          ),
                        ),
                      ),
                    ),
                  ),
                )),
          ],
        )
      ],
    );
  }
}
