import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:movie_booking/services/film_api.dart';
import 'package:velocity_x/velocity_x.dart';

class HomeHeader extends StatelessWidget {
  const HomeHeader({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              "Hi, Angelina"
                  .text
                  .size(18)
                  .color(AppColors.whiteTextColor)
                  .make(),
              10.heightBox,
              "Welcome back"
                  .text
                  .size(26)
                  .bold
                  .color(AppColors.whiteTextColor)
                  .make(),
            ],
          ),
          GestureDetector(
            onTap: () {
              FilmApi.fetchFilms();
            },
            child: Icon(Icons.notifications_none,
                color: AppColors.whiteTextColor, size: 30),
          ),
        ],
      ),
    );
  }
}
