
import 'package:get/get.dart';
import 'package:movie_booking/helper/gen_uuid.dart';
import 'package:movie_booking/model/film.dart';
import 'package:movie_booking/model/service.dart';
import 'package:movie_booking/services/film_api.dart';

class HomePageController extends GetxController {

  List listFilm = [];
  var isLoading = true.obs;

  @override
  void onInit() async {
    listFilm = await FilmApi.fetchFilms() as List;
    isLoading.value = false;
    print("listFilm: ${listFilm[0]}");
    super.onInit();
  }

  // List<Film> listNewFilm = [
  //   Film(
  //     id: GenerateUuid.generate(),
  //     image: "assets/images/thumb_quantumania.png",
  //     title: "Quantumania",
  //     duration: "2h30m",
  //     movieGenre: ["Action", "Adventure", "Fantasy"],
  //     censorships: 18,
  //     desc: "In a world where technology controls society, a rogue hacker and a gifted mechanic team up to dismantle the oppressive regime's surveillance network. As they navigate through a maze of digital traps and human betrayal, they discover a secret that could change the fate of humanity. 'Digital Uprising' is a gripping sci-fi thriller that challenges our perceptions of freedom, identity, and the power of information.",
  //     director: "Jame Cameron",
  //     directorImage: "assets/images/director_img.png",
  //     time: "20.12.2024",
  //     rate: 8.5,
  //     popular: 100,
  //   ),
  //   Film(
  //     id: GenerateUuid.generate(),
  //     image: "assets/images/thumb_quantumania.png",
  //     title: "Quantumania",
  //     duration: "2h30m",
  //     movieGenre: ["Action", "Adventure", "Fantasy"],
  //     censorships: 18,
  //     desc: "In a world where technology controls society, a rogue hacker and a gifted mechanic team up to dismantle the oppressive regime's surveillance network. As they navigate through a maze of digital traps and human betrayal, they discover a secret that could change the fate of humanity. 'Digital Uprising' is a gripping sci-fi thriller that challenges our perceptions of freedom, identity, and the power of information.",
  //     director: "Jame Cameron",
  //     directorImage: "assets/images/director_img.png",
  //     time: "20.12.2024",
  //     rate: 8.5,
  //     popular: 100,
  //   ),
  //   Film(
  //     id: GenerateUuid.generate(),
  //     image: "assets/images/thumb_quantumania.png",
  //     title: "Quantumania",
  //     duration: "2h30m",
  //     movieGenre: ["Action", "Adventure", "Fantasy"],
  //     censorships: 18,
  //     desc: "In a world where technology controls society, a rogue hacker and a gifted mechanic team up to dismantle the oppressive regime's surveillance network. As they navigate through a maze of digital traps and human betrayal, they discover a secret that could change the fate of humanity. 'Digital Uprising' is a gripping sci-fi thriller that challenges our perceptions of freedom, identity, and the power of information.",
  //     director: "Jame Cameron",
  //     directorImage: "assets/images/director_img.png",
  //     time: "20.12.2024",
  //     rate: 8.5,
  //     popular: 100,
  //   ),
  //   Film(
  //     id: GenerateUuid.generate(),
  //     image: "assets/images/thumb_quantumania.png",
  //     title: "Quantumania",
  //     duration: "2h30m",
  //     movieGenre: ["Action", "Adventure", "Fantasy"],
  //     censorships: 18,
  //     desc: "In a world where technology controls society, a rogue hacker and a gifted mechanic team up to dismantle the oppressive regime's surveillance network. As they navigate through a maze of digital traps and human betrayal, they discover a secret that could change the fate of humanity. 'Digital Uprising' is a gripping sci-fi thriller that challenges our perceptions of freedom, identity, and the power of information.",
  //     director: "Jame Cameron",
  //     directorImage: "assets/images/director_img.png",
  //     time: "20.12.2024",
  //     rate: 8.5,
  //     popular: 100,
  //   ),
  // ];

  List promoList = [
    "assets/images/discount.png",
    "assets/images/beta_discount.jpeg",
    "assets/images/bhd_discount.jpeg",
    "assets/images/galaxy_discount.jpeg",
    "assets/images/lotte_discount.webp",
  ];

  List<Service> listService = [
    Service(
      image: "assets/images/service_retal.png",
      service: "Retal",
    ),
    Service(
      image: "assets/images/service_imax.png",
      service: "Imax",
    ),
    Service(
      image: "assets/images/service_4dx.png",
      service: "4DX",
    ),
    Service(
      image: "assets/images/service_sweetbox.png",
      service: "Sweetbox",
    ),
  ];

  var swiperIndex = 0.obs;

  void onSwiperIndexChanged(int index) {
    swiperIndex.value = index;
  }
}