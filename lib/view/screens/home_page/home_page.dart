import 'package:card_swiper/card_swiper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:movie_booking/services/film_api.dart';
import 'package:movie_booking/view/common_widgets/seall_widget.dart';
import 'package:movie_booking/view/screens/home_page/home_page_controller.dart';
import 'package:movie_booking/view/screens/home_page/parts/coming_soon.dart';
import 'package:movie_booking/view/screens/home_page/parts/home_header.dart';
import 'package:movie_booking/view/screens/home_page/parts/home_search.dart';
import 'package:movie_booking/view/screens/home_page/parts/new_movie.dart';
import 'package:movie_booking/view/screens/home_page/parts/now_playing.dart';
import 'package:movie_booking/view/screens/home_page/parts/promo_discount.dart';
import 'package:movie_booking/view/screens/home_page/parts/service.dart';
import 'package:movie_booking/view/screens/home_page/widgets/film_card.dart';
import 'package:movie_booking/view/screens/home_page/widgets/film_card_property.dart';
import 'package:movie_booking/view/screens/home_page/widgets/part_title.dart';
import 'package:movie_booking/view/screens/home_page/widgets/service_item.dart';
import 'package:movie_booking/view/screens/movies/movies.dart';
import 'package:movie_booking/view_model/film_vm.dart';
import 'package:velocity_x/velocity_x.dart';

class HomePage extends StatelessWidget {
  HomePage({super.key});

  final homepageCtrl = Get.find<HomePageController>();
  // final filmVM = Get.find<FilmViewModel>();

  @override
  Widget build(BuildContext context) {
    return Container(
      // padding: EdgeInsets.symmetric(horizontal: 16),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // Header
            Obx(
              () => homepageCtrl.isLoading.value
                  ? CircularProgressIndicator().centered()
                  : Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        HomeHeader(),
                        // Search
                        20.heightBox,
                        HomeSearch(),
                        // Now Playing
                        32.heightBox,
                        HomeNowPlaying(),
                        // Upcoming
                        32.heightBox,
                        HomeComingSoon(),
                        // Discount
                        32.heightBox,
                        PromoAndDiscount(),
                        // Service
                        32.heightBox,
                        HomepageService(),
                        // Movie new
                        32.heightBox,
                        HomeNewMovie(),
                      ],
                    ),
            ),
          ],
        ),
      ),
    );
  }
}
