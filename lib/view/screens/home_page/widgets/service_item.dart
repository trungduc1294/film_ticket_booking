import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:velocity_x/velocity_x.dart';

class ServiceItem extends StatelessWidget {

  String service;
  String image_url;
  Function()? onTap;

  ServiceItem({required this.service, required this.image_url, this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              width: 80,
              height: 80,
              child: CircleAvatar(
                radius: 100,
                child: Image.asset(image_url, fit: BoxFit.fill),
              ),
            ),
            20.heightBox,
            service.text.size(16).color(AppColors.whiteTextColor).medium.make(),
          ],
        ),
      ),
    );
  }
}