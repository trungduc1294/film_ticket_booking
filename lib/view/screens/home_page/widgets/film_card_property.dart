import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:velocity_x/velocity_x.dart';

class FilmCardProperty extends StatelessWidget {

  IconData icon;
  String text;

  FilmCardProperty({required this.icon, required this.text});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(icon, color: AppColors.greyTextColor, size: 16),
        4.widthBox,
        SizedBox(
          width: MediaQuery.of(context).size.width * 0.38 - 50,
          child: Text(
            text,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              color: AppColors.greyTextColor,
              fontSize: 12,
            ),
          ),
        ),
      ],
    );
  }
}
