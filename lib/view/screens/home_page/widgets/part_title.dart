import 'package:flutter/cupertino.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:movie_booking/view/common_widgets/seall_widget.dart';
import 'package:velocity_x/velocity_x.dart';

class PartTitle extends StatelessWidget {

  String title;
  Function()? onTap;

  PartTitle({required this.title, this.onTap});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          title
              .text
              .size(24)
              .bold
              .color(AppColors.whiteTextColor)
              .make(),
          SeeAllWidget(
            onTap: onTap,
          ),
        ],
      ),
    );
  }
}
