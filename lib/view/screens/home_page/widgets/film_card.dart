import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:movie_booking/view/screens/home_page/widgets/film_card_property.dart';
import 'package:velocity_x/velocity_x.dart';

class FilmCard extends StatelessWidget {

  String id;
  String image;
  String name;
  String genre;
  String time;

  FilmCard({required this.id ,required this.image, required this.name, required this.genre, required this.time});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Get.toNamed('/movie_detail');
      },
      child: Container(
        // margin: EdgeInsets.only(right: 16),
        width: MediaQuery.of(context).size.width * 0.38,
        
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Image(image: NetworkImage("$image"),height: 250 , fit: BoxFit.cover),
            16.heightBox,
            Text(
              name,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                color: AppColors.primary,
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
            8.heightBox,
            FilmCardProperty(
                icon: Icons.videocam_outlined, text: genre),
            FilmCardProperty(
                icon: Icons.calendar_month_outlined, text: time)
          ],
        ),
      ),
    );
  }
}
