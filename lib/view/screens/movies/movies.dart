import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/route_manager.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:movie_booking/view/screens/home_page/home_page_controller.dart';
import 'package:movie_booking/view/screens/home_page/widgets/film_card.dart';
import 'package:movie_booking/view/screens/movies/movies_controller.dart';
import 'package:movie_booking/view/screens/movies/parts/comming_soon.dart';
import 'package:movie_booking/view/screens/movies/parts/nowplaying.dart';
import 'package:velocity_x/velocity_x.dart';

class Movies extends StatelessWidget {
  Movies({super.key});

  final moviesCtrl = Get.put(MoviesController());
  final homepageCtrl = Get.find<HomePageController>();

  final List<Widget> _pages = [
    NowPlaying(),
    CommingSoon(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                decoration: BoxDecoration(
                    color: AppColors.darkGreyBackground,
                    borderRadius: BorderRadius.circular(10)),
                child: Row(
                  children: [
                    Expanded(
                      child: Obx(() => GestureDetector(
                            onTap: () {
                              moviesCtrl.changeTopBarIndex(0);
                            },
                            child: Container(
                              padding: EdgeInsets.symmetric(vertical: 10),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                color: moviesCtrl.currentTopBarIndex == 0
                                    ? AppColors.primary
                                    : Colors.transparent,
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: "Now Playing"
                                  .text
                                  .size(18)
                                  .bold
                                  .color(moviesCtrl.currentTopBarIndex == 0
                                      ? Colors.black
                                      : AppColors.whiteTextColor)
                                  .make(),
                            ),
                          )),
                    ),
                    Expanded(
                      child: Obx(() => GestureDetector(
                            onTap: () {
                              moviesCtrl.changeTopBarIndex(1);
                            },
                            child: Container(
                              padding: EdgeInsets.symmetric(vertical: 10),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                color: moviesCtrl.currentTopBarIndex == 1
                                    ? AppColors.primary
                                    : Colors.transparent,
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: "Comming Soon"
                                  .text
                                  .size(18)
                                  .bold
                                  .color(moviesCtrl.currentTopBarIndex == 1
                                      ? Colors.black
                                      : AppColors.whiteTextColor)
                                  .make(),
                            ),
                          )),
                    )
                  ],
                ),
              ),

              // Movie
              36.heightBox,
              Obx(
                () => _pages[moviesCtrl.currentTopBarIndex.value],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
