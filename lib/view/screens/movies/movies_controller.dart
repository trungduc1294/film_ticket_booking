import 'package:get/get.dart';

class MoviesController extends GetxController {

  var currentTopBarIndex = 0.obs; // 0 is now playing, 1 is comming soon

  void changeTopBarIndex(int index) {
    currentTopBarIndex.value = index;
    print("currentTopBarIndex: ${currentTopBarIndex.value}");
  }
  
}