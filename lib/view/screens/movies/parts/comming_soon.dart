import 'package:flutter/material.dart';
import 'package:movie_booking/view/screens/home_page/widgets/film_card.dart';

class CommingSoon extends StatelessWidget {
  const CommingSoon({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.symmetric(horizontal: 32, vertical: 10),
      
      child: Wrap(
        alignment: WrapAlignment.spaceBetween,
        runSpacing: 8,
        children: List.generate(
            5,
            (index) => FilmCard(
                id: "1",
                image:
                    "https://firebasestorage.googleapis.com/v0/b/movie-booking-a09fe.appspot.com/o/thumb_guadian.jpg?alt=media&token=b315fc30-2098-4057-a1e0-dc943389e55f",
                name: "Avenger Infinity",
                genre: "Action",
                time: "2h30p")),
      ),
    );
  }
}


