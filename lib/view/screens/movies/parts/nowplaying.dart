import 'package:flutter/material.dart';
import 'package:movie_booking/view/screens/home_page/widgets/film_card.dart';

class NowPlaying extends StatelessWidget {
  const NowPlaying({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.symmetric(horizontal: 32, vertical: 10),
      
      child: Wrap(
        alignment: WrapAlignment.spaceBetween,
        runSpacing: 8,
        children: List.generate(
            5,
            (index) => FilmCard(
                id: "1",
                image:
                    "https://firebasestorage.googleapis.com/v0/b/movie-booking-a09fe.appspot.com/o/thumb_avenger_infinity.png?alt=media&token=d866f158-a1d6-4684-884c-39c3e57187d3",
                name: "Avenger Infinity",
                genre: "Action",
                time: "2h30p")),
      ),
    );
  }
}
