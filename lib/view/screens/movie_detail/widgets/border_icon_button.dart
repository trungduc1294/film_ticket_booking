import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movie_booking/const/colors.dart';

class BorderIconButton extends StatelessWidget {

  IconData icon;
  String text;

  BorderIconButton({required this.icon, required this.text});

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      decoration: BoxDecoration(
        color: Colors.transparent,
        borderRadius: BorderRadius.circular(5),
        border: Border.all(color: AppColors.whiteTextColor),
      ),
      child: Row(
        children: [
          Icon(icon, color: AppColors.whiteTextColor,),
          Text(text, style: TextStyle(color: AppColors.whiteTextColor),),
        ],
      )
    );
  }
}