import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:movie_booking/view/screens/movie_detail/movie_detail_controller.dart';
import 'package:velocity_x/velocity_x.dart';

class Cinema extends StatelessWidget {

  int index;
  bool isSelected;
  String cinemaName;
  String cinemaLocation;
  String cinemaDistance;
  String cinemaLogo;

  Cinema({
    required this.index,
    required this.isSelected,
    required this.cinemaName,
    required this.cinemaLocation,
    required this.cinemaLogo,
    required this.cinemaDistance,
  });

  final movieDetailCtrl = Get.find<MovieDetailController>();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        movieDetailCtrl.selectCinema(index);
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.all(16),
        margin: EdgeInsets.only(bottom: 16),
        decoration: BoxDecoration(
          color: isSelected ? AppColors.innerBackground : AppColors.darkGreyBackground,
          borderRadius: BorderRadius.circular(12),
          border: Border.all(
            color: isSelected ? AppColors.primary : AppColors.darkGreyBackground,
            width: 1,
          ),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    cinemaName,
                    style: TextStyle(
                      color: AppColors.whiteTextColor,
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  Row(
                    children: [
                      cinemaDistance.text.size(16).color(AppColors.whiteTextColor).make(),
                      8.widthBox,
                      Expanded(
                        child: cinemaLocation.text.size(16).overflow(TextOverflow.ellipsis).color(AppColors.whiteTextColor).make(),
                      )
                    ],
                  )
                ],
              ),
            ),
            Image.asset(
              cinemaLogo,
            ),
          ],
        ),
      ),
    );
  }
}