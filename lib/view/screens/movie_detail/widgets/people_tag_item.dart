import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:velocity_x/velocity_x.dart';

class PeopleTagItem extends StatelessWidget {

  String imageUrl;
  String name;

  PeopleTagItem({required this.imageUrl, required this.name});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 16),
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(
        color: AppColors.darkGreyBackground,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Row(
        children: [
          Container(
            width: 36,
            height: 36,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(36),
              image: DecorationImage(
                image: AssetImage(imageUrl),
                fit: BoxFit.cover,
              ),
            ),
          ),
          12.widthBox,
          name.text.size(14).semiBold.color(AppColors.whiteTextColor).make(),
        ],
      ),
    );
  }
}