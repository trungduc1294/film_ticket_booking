import 'package:flutter/cupertino.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:velocity_x/velocity_x.dart';

class DetailItem extends StatelessWidget {

  String title;
  String content;

  DetailItem({required this.title, required this.content});

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          width: 130,
          child: title.text.size(16).color(AppColors.greyTextColor).make(),
        ),
        SizedBox(
          width: MediaQuery.of(context).size.width - 130 - 16*2,
          child: content.text.size(16).overflow(TextOverflow.ellipsis).semiBold.color(AppColors.whiteTextColor).make(),
        )
      ],
    );
  }
}