import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:movie_booking/view/common_widgets/primary_button.dart';
import 'package:movie_booking/view/screens/movie_detail/movie_detail_controller.dart';
import 'package:movie_booking/view/screens/movie_detail/parts/actor.dart';
import 'package:movie_booking/view/screens/movie_detail/parts/choose_cinema.dart';
import 'package:movie_booking/view/screens/movie_detail/parts/director.dart';
import 'package:movie_booking/view/screens/movie_detail/parts/movie_detail_header.dart';
import 'package:movie_booking/view/screens/movie_detail/parts/movie_info.dart';
import 'package:movie_booking/view/screens/movie_detail/widgets/border_icon_button.dart';
import 'package:movie_booking/view/screens/movie_detail/widgets/cinema.dart';
import 'package:movie_booking/view/screens/movie_detail/widgets/detail_item.dart';
import 'package:movie_booking/view/screens/movie_detail/widgets/people_tag_item.dart';
import 'package:velocity_x/velocity_x.dart';

class MovieDetail extends StatelessWidget {
  MovieDetail({super.key});

  final movieDetailCtrl = Get.find<MovieDetailController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            MovieDetailHeader(),
            // Movie info
            32.heightBox,
            MovieInfo(),
            // Director
            32.heightBox,
            Director(),
            // Cast
            32.heightBox,
            Actor(),
            // choose cinema
            32.heightBox,
            ChooseCinema(),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
              child: PrimaryButton(
                btnText: "Continue",
                onTapFunction: () {
                  Get.toNamed("/select_seat");
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
