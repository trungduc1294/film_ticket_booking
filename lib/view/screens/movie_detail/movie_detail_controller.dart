import 'package:get/get.dart';

class MovieDetailController extends GetxController {
  
  var selectedCinemaIndex = 0.obs;

  void selectCinema(int index) {
    selectedCinemaIndex.value = index;
    print("Selected cinema index: $selectedCinemaIndex");
  }

  List cinemaList = [
    {
      "cinemaName": "Vincom Ocean Park CGV",
      "cinemaLocation": "Da Ton, Gia Lam, Ha Noi",
      "cinemaDistance": "4.55 km",
      "cinemaLogo": "assets/icons/cgv_rounded.png",
    },
    {
      "cinemaName": "Aeon Mall CGV",
      "cinemaLocation": "27 Co Linh, Long Bien, Ha Noi",
      "cinemaDistance": "9.55 km",
      "cinemaLogo": "assets/icons/cgv_rounded.png",
    },
    {
      "cinemaName": "Lotte Cinema Long Bien",
      "cinemaLocation": "7-9 Nguyen Van Linh, Long Bien, Ha Noi",
      "cinemaDistance": "4.55 km",
      "cinemaLogo": "assets/icons/lotte_icon.png",
    },
  ];
}