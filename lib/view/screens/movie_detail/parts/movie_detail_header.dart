import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:movie_booking/view/screens/movie_detail/widgets/border_icon_button.dart';
import 'package:velocity_x/velocity_x.dart';

class MovieDetailHeader extends StatelessWidget {
  const MovieDetailHeader({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.4,
      width: double.infinity,
      // color: Colors.red,
      child: Stack(
        children: [
          Container(
            margin: EdgeInsets.only(bottom: 100),
            child: Image(
              image: AssetImage('assets/images/thumb_avenger.png'),
              fit: BoxFit.cover,
              width: double.infinity,
              height: double.infinity,
            ),
          ),
          Positioned(
            top: 80,
            left: 16,
            child: Container(
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: AppColors.whiteTextColor.withOpacity(0.3),
                borderRadius: BorderRadius.circular(5),
              ),
              child: IconButton(
                icon: Icon(
                  Icons.arrow_back,
                  color: AppColors.whiteTextColor,
                ),
                onPressed: () {
                  Get.back();
                },
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.all(20),
            margin: EdgeInsets.only(
              top: MediaQuery.of(context).size.height * 0.19,
              left: 16,
              right: 16,
            ),
            decoration: BoxDecoration(
              color: AppColors.darkGreyBackground,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Avengers: Infinity War",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: AppColors.whiteTextColor,
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Row(
                  children: [
                    "2h29m".text.color(AppColors.greyTextColor).size(16).make(),
                    10.widthBox,
                    "16.02.2024"
                        .text
                        .color(AppColors.greyTextColor)
                        .size(16)
                        .make(),
                  ],
                ),
                Spacer(),
                Row(
                  children: [
                    "Review"
                        .text
                        .size(16)
                        .color(AppColors.whiteTextColor)
                        .bold
                        .make(),
                    8.widthBox,
                    Icon(
                      Icons.star,
                      color: AppColors.primary,
                      size: 16,
                    ),
                    4.widthBox,
                    "4.5"
                        .text
                        .size(16)
                        .color(AppColors.whiteTextColor)
                        .bold
                        .make(),
                    4.widthBox,
                    "(20000)"
                        .text
                        .size(12)
                        .color(AppColors.greyTextColor)
                        .make(),
                  ],
                ),
                12.heightBox,
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    //rating star
                    Row(
                      children: List.generate(
                          5,
                          (index) => Icon(
                                Icons.star,
                                color: AppColors.greyTextColor,
                                size: 32,
                              )),
                    ),
                    // Watch now button
                    BorderIconButton(
                      icon: Icons.play_arrow,
                      text: "Watch trailer",
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
