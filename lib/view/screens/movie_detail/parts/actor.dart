import 'package:flutter/cupertino.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:movie_booking/view/screens/movie_detail/widgets/people_tag_item.dart';
import 'package:velocity_x/velocity_x.dart';

class Actor extends StatelessWidget {
  const Actor({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            "Actor".text.size(24).bold.color(AppColors.whiteTextColor).make(),
            20.heightBox,
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: List.generate(
                  3,
                  (index) => PeopleTagItem(
                    name: 'Anthony Russo',
                    imageUrl: 'assets/images/athony_russo.jpeg',
                  ),
                ),
              ),
            ),
          ],
        ));
  }
}
