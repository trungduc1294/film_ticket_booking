import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:movie_booking/view/screens/movie_detail/movie_detail_controller.dart';
import 'package:movie_booking/view/screens/movie_detail/widgets/cinema.dart';
import 'package:velocity_x/velocity_x.dart';

class ChooseCinema extends StatelessWidget {
  ChooseCinema({super.key});

  final movieDetailCtrl = Get.find<MovieDetailController>();

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            "Cinema".text.size(24).bold.color(AppColors.whiteTextColor).make(),
            20.heightBox,
            Obx(() => Column(
                  children: List.generate(3, (index) {
                    return Cinema(
                      index: index,
                      isSelected:
                          movieDetailCtrl.selectedCinemaIndex.value == index,
                      cinemaName: movieDetailCtrl.cinemaList[index]
                          ["cinemaName"],
                      cinemaLocation: movieDetailCtrl.cinemaList[index]
                          ["cinemaLocation"],
                      cinemaDistance: movieDetailCtrl.cinemaList[index]
                          ["cinemaDistance"],
                      cinemaLogo: movieDetailCtrl.cinemaList[index]
                          ["cinemaLogo"],
                    );
                  }),
                )),
          ],
        ));
  }
}
