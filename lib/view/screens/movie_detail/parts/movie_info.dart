import 'package:flutter/cupertino.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:movie_booking/view/screens/movie_detail/widgets/detail_item.dart';
import 'package:velocity_x/velocity_x.dart';

class MovieInfo extends StatelessWidget {
  const MovieInfo({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          DetailItem(
            title: 'Movie genre:',
            content: 'Action, Adventure, Fantasy',
          ),
          16.heightBox,
          DetailItem(
            title: 'Censorship:',
            content: '13+',
          ),
          16.heightBox,
          DetailItem(
            title: 'Language:',
            content: 'English',
          ),
          32.heightBox,
          "Storyline".text.size(24).bold.color(AppColors.whiteTextColor).make(),
          20.heightBox,
          "After the devastating events of Avengers: Infinity War (2018), the universe is in ruins. With the help of remaining allies, the Avengers assemble once more in order to reverse Thanos' actions and restore balance to the universe."
              .text
              .size(16)
              .color(AppColors.whiteTextColor)
              .make(),
        ],
      ),
    );
  }
}
