import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';
import 'package:get/route_manager.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:movie_booking/view/screens/inpoint/inpoint_controller.dart';
import 'package:movie_booking/locator.dart';
import 'package:movie_booking/view/common_widgets/custom_checkbox.dart';
import 'package:movie_booking/view/common_widgets/primary_button.dart';
import 'package:movie_booking/view/common_widgets/secondary_button.dart';
import 'package:movie_booking/view/common_widgets/swipper_dot.dart';
import 'package:movie_booking/view/screens/inpoint/widgets/language_item.dart';
import 'package:movie_booking/view/screens/inpoint/widgets/pageview_item.dart';
import 'package:velocity_x/velocity_x.dart';

class Inpoint extends StatelessWidget {
  Inpoint({super.key});

  @override
  Widget build(BuildContext context) {
    var inpointScreenCtrl = locator<InpointController>();

    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            children: [
              Row(
                children: [
                  Image(image: AssetImage('assets/images/app_logo.png')),
                  Spacer(),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 9),
                    decoration: BoxDecoration(
                      color: Colors.transparent,
                      borderRadius: BorderRadius.circular(20),
                      border: Border.all(
                          color: AppColors.lightGreyTextColor, width: 1),
                    ),
                    child: GestureDetector(
                      onTap: () {
                        // show bottom sheet
                        _showModalBottomSheet(context);
                      },
                      child: Row(
                        children: [
                          Icon(Icons.translate,
                              color: AppColors.lightGreyTextColor, size: 20),
                          4.widthBox,
                          "English"
                              .text
                              .size(12)
                              .medium
                              .color(AppColors.lightGreyTextColor)
                              .make(),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              80.heightBox,
              Container(
                height: 460,
                child: PageView.builder(
                  onPageChanged: inpointScreenCtrl.onPageChanged,
                  itemCount: 3,
                  itemBuilder: (context, index) {
                    return PageViewItem();
                  },
                ),
              ),
              Obx(() => Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SwipperDot(
                          index: 0,
                          isSelected:
                              inpointScreenCtrl.selectedPageIndex.value == 0),
                      8.widthBox,
                      SwipperDot(
                          index: 1,
                          isSelected:
                              inpointScreenCtrl.selectedPageIndex.value == 1),
                      8.widthBox,
                      SwipperDot(
                          index: 2,
                          isSelected:
                              inpointScreenCtrl.selectedPageIndex.value == 2),
                    ],
                  )),
              40.heightBox,
              PrimaryButton(
                btnText: "Sign in",
                onTapFunction: () {
                  Get.offAllNamed("/main");
                },
              ),
              16.heightBox,
              SecondaryButton(
                btnText: "Sign up", 
                onTapFunction: () {
                  Get.toNamed("/signup");
                }
              ),
              Spacer(),
              // "By signing in, you agree to our Terms of Service and Privacy Policy"
              //     .text
              //     .align(TextAlign.center)
              //     .size(12)
              //     .medium
              //     .color(AppColors.greyTextColor)
              //     .makeCentered(),
            ],
          ),
        ),
      ),
    );
  }

  void _showModalBottomSheet(BuildContext context) {
    var inpointScreenCtrl = locator<InpointController>();
    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 20),
          decoration: BoxDecoration(
            color: AppColors.darkGreyBackground,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20),
            ),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              "Choose language"
                  .text
                  .color(AppColors.whiteTextColor)
                  .size(26)
                  .bold
                  .make(),
              10.heightBox,
              "Which language do you want to use for your MBooking experience?"
                  .text
                  .color(AppColors.whiteTextColor)
                  .size(12)
                  .make(),
              20.heightBox,
              Obx(() => Column(
                    children: [
                      LanguageItem(
                        languageIndex: 0,
                        languageName: "English",
                        isSelected:
                            inpointScreenCtrl.languageSelectedIndex.value == 0,
                      ),
                      10.heightBox,
                      Divider(
                        color: AppColors.greyTextColor.withOpacity(0.2),
                      ),
                      10.heightBox,
                      LanguageItem(
                        languageIndex: 1,
                        languageName: "Vietnamese",
                        isSelected:
                            inpointScreenCtrl.languageSelectedIndex.value == 1,
                      ),
                    ],
                  ))
            ],
          ),
        );
      },
    );
  }
}
