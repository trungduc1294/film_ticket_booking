import 'package:get/get.dart';

class InpointController extends GetxController {
  var selectedPageIndex = 0.obs;
  var languageSelectedIndex = 0.obs; // 0 is english, 1 is vietnamese

  void onPageChanged(int index) {
    selectedPageIndex.value = index;
    print(selectedPageIndex.value);
  }

  void onLanguageSelected(int index) {
    languageSelectedIndex.value = index;
    print(languageSelectedIndex.value);
  }
}