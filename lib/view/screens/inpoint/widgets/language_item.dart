import 'package:flutter/cupertino.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:movie_booking/view/screens/inpoint/inpoint_controller.dart';
import 'package:movie_booking/locator.dart';
import 'package:movie_booking/view/common_widgets/custom_checkbox.dart';
import 'package:velocity_x/velocity_x.dart';

class LanguageItem extends StatelessWidget {

  int languageIndex;
  String languageName;
  bool isSelected;

  LanguageItem({required this.languageIndex,required this.languageName,required this.isSelected});

  @override
  Widget build(BuildContext context) {
    var inpointScreenCtrl = locator<InpointController>();

    return GestureDetector(
      onTap: () {
        inpointScreenCtrl.onLanguageSelected(languageIndex);
      },
      child: Row(
        children: [
          languageName.text.color(isSelected ? AppColors.primary : AppColors.whiteTextColor).size(18).bold.make(),
          Spacer(),
          CustomCheckbox(isSelected: isSelected),
        ],
      ),
    );
  }
}
