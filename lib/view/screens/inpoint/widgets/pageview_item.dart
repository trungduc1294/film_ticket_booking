import 'package:flutter/material.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:velocity_x/velocity_x.dart';

class PageViewItem extends StatelessWidget {
  const PageViewItem({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Image(image: AssetImage('assets/images/thumb_avenger.png')),
          30.heightBox,
          "MBooking hello!"
              .text
              .size(22)
              .color(AppColors.whiteTextColor)
              .bold
              .make(),
          10.heightBox,
          "Enjoy your favorite movies"
              .text
              .size(12)
              .color(AppColors.whiteTextColor)
              .make(),
        ],
      ),
    );
  }
}
