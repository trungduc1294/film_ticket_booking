import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:movie_booking/view/common_widgets/custom_text_form_field.dart';
import 'package:movie_booking/view/common_widgets/primary_button.dart';
import 'package:movie_booking/view/screens/enter_username/enter_username_controller.dart';
import 'package:velocity_x/velocity_x.dart';

class EnterUsername extends StatelessWidget {
  const EnterUsername({super.key});

  @override
  Widget build(BuildContext context) {

    final enterUsernameCtrl = Get.find<EnterUsernameController>();

    return Scaffold(
      appBar: AppBar(
        title: Text(''),
      ),
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              56.heightBox,
              "Enter Username"
                  .text
                  .size(32)
                  .color(AppColors.primary)
                  .bold
                  .make(),
              "Latin characters, no emoji/symbols"
                  .text
                  .size(16)
                  .color(AppColors.greyTextColor)
                  .make(),
              CustomTFF(
                labelText: "Username",
                controller: enterUsernameCtrl.username,
              ),
              Spacer(),
              PrimaryButton(
                btnText: "Done",
                onTapFunction: () {
                  enterUsernameCtrl.printUsername();
                  if (enterUsernameCtrl.checkUsername()) {
                    Get.offAllNamed("/main");
                  }
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}