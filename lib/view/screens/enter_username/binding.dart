import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/bindings_interface.dart';
import 'package:movie_booking/view/screens/enter_username/enter_username_controller.dart';

class EnterUsernameBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<EnterUsernameController>(() => EnterUsernameController());
  }
}