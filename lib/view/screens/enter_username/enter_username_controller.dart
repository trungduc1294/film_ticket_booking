import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:get/route_manager.dart';
import 'package:movie_booking/const/colors.dart';

class EnterUsernameController extends GetxController {
  TextEditingController username = TextEditingController();

  bool checkUsername() {
    if (username.text.isEmpty) {
      Get.snackbar("Error", "Username cannot be empty", colorText: AppColors.whiteTextColor);
      return false;
    }
    if (username.text.length < 6) {
      Get.snackbar("Error", "Username must be at least 6 characters", colorText: AppColors.whiteTextColor);
      return false;
    }
    return true;
  }

  void printUsername() {
    print(username.text);
  }
}