import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/route_manager.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:movie_booking/view/common_widgets/film_card_portrait.dart';
import 'package:movie_booking/view/screens/tickets/tickets_controller.dart';
import 'package:velocity_x/velocity_x.dart';

class Tickets extends StatelessWidget {
  Tickets({super.key});

  final ticketsCtrl = Get.put(TicketsController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Obx(
          () => ticketsCtrl.isLoading.value
            ? CircularProgressIndicator().centered()
            : Column(
              children: [
                "My Tickets".text.size(24).bold.color(AppColors.whiteTextColor).make(),
                24.heightBox,
                Expanded(
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    // child: ListView(
                    //   children: [
                    //     FilmCardPortrait(
                    //     film_name: "Avenger Infinity War",
                    //     duration: "2h30'",
                    //     genre: "Action",
                    //     address: "Vincom Ocean Park CGV",
                    //     thumb: "assets/images/thumb_avenger_infinity.png"),
                    //   ],
                    // ),
                    child: ListView.builder(
                      itemCount: ticketsCtrl.listTicket.length,
                      itemBuilder: (context, index) {
                        return FilmCardPortrait(
                          ticketId: ticketsCtrl.listTicket[index].id,
                          film_name: ticketsCtrl.listTicket[index].filmName,
                          duration: ticketsCtrl.listTicket[index].duration,
                          genre: ticketsCtrl.listTicket[index].movieGenre.join(', '),
                          address: ticketsCtrl.listTicket[index].cinemaName,
                          thumb: ticketsCtrl.listTicket[index].filmThumbUrl
                        );
                      },
                    ),
                  )
                )
              ],
            ),
        )
      ),
    );
  }
}