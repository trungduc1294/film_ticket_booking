import 'package:get/get.dart';
import 'package:movie_booking/services/ticket_api.dart';

class TicketsController extends GetxController {


  List listTicket = [];
  var isLoading = true.obs;


  @override
  void onInit() async {
    listTicket = await TicketApi.fetchTickets() as List;
    isLoading.value = false;
    print("Ticket: ${listTicket[0].filmName}");
    print("Ticket: ${listTicket}");
    super.onInit();
  }

  getTicketById(String id) {
    return listTicket.firstWhere((ticket) => ticket.id == id);
  }
}