import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:velocity_x/velocity_x.dart';

class SeeAllWidget extends StatelessWidget {

  Function()? onTap;

  SeeAllWidget({this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Row(
        children: [
          "See All".text.size(14).color(AppColors.primary).make(),
          3.widthBox,
          Icon(Icons.arrow_forward_ios, color: AppColors.primary, size: 14),
        ],
      ),
    );
  }
}
