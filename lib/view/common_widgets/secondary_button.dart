import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:velocity_x/velocity_x.dart';

class SecondaryButton extends StatelessWidget {

  String btnText;
  double width;
  Function()? onTapFunction;

  SecondaryButton({required this.btnText, this.width = 0, this.onTapFunction});


  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTapFunction,
      child: Container(
        width: width == 0 ? context.screenWidth : width,
        padding: EdgeInsets.symmetric(vertical: 18),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: Colors.transparent,
          borderRadius: BorderRadius.circular(100),
          border: Border.all(color: AppColors.whiteTextColor, width: 1),
        ),
        child: btnText.text.size(20).bold.color(AppColors.whiteTextColor).make(),
      ),
    );
  }
}