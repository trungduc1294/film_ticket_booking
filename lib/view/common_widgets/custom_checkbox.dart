import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movie_booking/const/colors.dart';

class CustomCheckbox extends StatelessWidget {

  bool isSelected = false;

  CustomCheckbox({required this.isSelected});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(4),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(100),
        border: Border.all(color: isSelected ? AppColors.primary : AppColors.whiteTextColor, width: 1),
      ),
      child: Container(
        height: 16,
        width: 16,
        decoration: BoxDecoration(
          color: isSelected ? AppColors.primary : Colors.transparent,
          borderRadius: BorderRadius.circular(100),
        ),
      ),
    );
  }
}
