import 'package:flutter/material.dart';
import 'package:movie_booking/const/colors.dart';

class SwipperDot extends StatelessWidget {
  int index;
  bool isSelected;

  SwipperDot({required this.index, required this.isSelected});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 8,
      height: 8,
      decoration: BoxDecoration(
        color: isSelected ? AppColors.primary : AppColors.greyTextColor,
        borderRadius: BorderRadius.circular(4),
      ),
    );
  }
}
