import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:movie_booking/view/screens/payment/widgets/film_detail_item.dart';
import 'package:velocity_x/velocity_x.dart';

class FilmCardPortrait extends StatelessWidget {
  String ticketId;
  String film_name;
  String duration;
  String genre;
  String address;
  String thumb;

  FilmCardPortrait(
      {required this.ticketId,
      required this.film_name,
      required this.duration,
      required this.genre,
      required this.address,
      required this.thumb});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Get.toNamed(
          "/ticket_detail",
          arguments: {
            "ticket_id": ticketId,
            "film_name": film_name,
            "duration": duration,
            "genre": genre,
            "address": address,
            "thumb": thumb,
          },
        );
      },
      child: Container(
        margin: EdgeInsets.only(bottom: 16),
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          color: AppColors.darkGreyBackground,
          borderRadius: BorderRadius.circular(16),
        ),
        child: Row(
          children: [
            Container(
              decoration: BoxDecoration(
                color: AppColors.darkGreyBackground,
                borderRadius: BorderRadius.circular(16),
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(16),
                  bottomLeft: Radius.circular(16),
                ),
                child: Image(
                  image: NetworkImage(thumb),
                  height: 150,
                ),
              ),
            ),
            16.widthBox,
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                film_name.text.size(20).bold.color(AppColors.primary).make(),
                16.heightBox,
                FilmDetailItem(icon: Icons.camera, text: genre),
                4.heightBox,
                FilmDetailItem(icon: Icons.location_on, text: address),
                4.heightBox,
                FilmDetailItem(icon: Icons.access_time, text: duration),
              ],
            )
          ],
        ),
      ),
    );
  }
}
