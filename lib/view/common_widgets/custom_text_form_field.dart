import 'package:flutter/material.dart';
import 'package:movie_booking/const/colors.dart';

class CustomTFF extends StatelessWidget {

  String labelText;
  Icon? prevIcon;
  TextEditingController? controller;

  CustomTFF({required this.labelText, this.prevIcon, this.controller});

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      decoration: InputDecoration(
        prefixIcon: prevIcon != null ? prevIcon : null,
        labelText: labelText,
        labelStyle: TextStyle(
          color: AppColors.whiteTextColor,
        ),
        prefixIconColor: AppColors.whiteTextColor,
        fillColor: AppColors.whiteTextColor,
        focusColor: AppColors.whiteTextColor,
      ),
      style: TextStyle(
        color: AppColors.whiteTextColor,
        fontSize: 24,
        fontWeight: FontWeight.w700,
      ),
    );
  }
}
