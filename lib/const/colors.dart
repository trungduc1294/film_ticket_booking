import 'dart:ui';

class AppColors {
  static const Color primary = Color(0xFFFCC434);
  static const Color blackBackground = Color(0xFF000000);
  static const Color darkGreyBackground = Color(0xFF1A1A1A);
  static const Color greyBackground = Color(0xFF3B3B3B);
  static const Color innerBackground = Color(0xFF261D08);

  static const Color whiteTextColor = Color(0xFFF2F2F2);
  static const Color blackTextColor = Color(0xFF000000);
  static const Color lightGreyTextColor = Color(0xFFE6E6E6);
  static const Color greyTextColor = Color(0xFFB3B3B3);
  static const Color secondaryGreyTextColor = Color(0xFFDEDEDE);

  static const Color blackIconColor = Color(0xFF292D32);
  static const Color darkButtonBg = Color(0xFF1A1A1A);

}