import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:movie_booking/const/colors.dart';
import 'package:movie_booking/locator.dart';
import 'package:movie_booking/view/screens/confirm_otp/binding.dart';
import 'package:movie_booking/view/screens/confirm_otp/confirm_opt.dart';
import 'package:movie_booking/view/screens/enter_username/binding.dart';
import 'package:movie_booking/view/screens/enter_username/enter_username.dart';
import 'package:movie_booking/view/screens/home_page/binding.dart';
import 'package:movie_booking/view/screens/home_page/home_page.dart';
import 'package:movie_booking/view/screens/inpoint/binding.dart';
import 'package:movie_booking/view/screens/inpoint/inpoint.dart';
import 'package:movie_booking/view/screens/main_screen/binding.dart';
import 'package:movie_booking/view/screens/main_screen/main_screen.dart';
import 'package:movie_booking/view/screens/movie_detail/binding.dart';
import 'package:movie_booking/view/screens/movie_detail/movie_detail.dart';
import 'package:movie_booking/view/screens/movies/binding.dart';
import 'package:movie_booking/view/screens/movies/movies.dart';
import 'package:movie_booking/view/screens/payment/binding.dart';
import 'package:movie_booking/view/screens/payment/payment.dart';
import 'package:movie_booking/view/screens/profile/binding.dart';
import 'package:movie_booking/view/screens/profile/profile.dart';
import 'package:movie_booking/view/screens/select_seat/binding.dart';
import 'package:movie_booking/view/screens/select_seat/select_seat.dart';
import 'package:movie_booking/view/screens/signup/binding.dart';
import 'package:movie_booking/view/screens/signup/signup.dart';
import 'package:movie_booking/view/screens/ticket_detail/binding.dart';
import 'package:movie_booking/view/screens/ticket_detail/ticket_detail.dart';
import 'package:movie_booking/view/screens/tickets/binding.dart';
import 'package:movie_booking/view/screens/tickets/tickets.dart';

void main() {
  setupLocator();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        appBarTheme: AppBarTheme(
          titleTextStyle: TextStyle(
            color: AppColors.whiteTextColor,
            fontSize: 20,
            fontWeight: FontWeight.w700,
          ),
          iconTheme: IconThemeData(color: AppColors.whiteTextColor),
          backgroundColor: AppColors.blackBackground,
          elevation: 0,
        ),
        bottomNavigationBarTheme: BottomNavigationBarThemeData(
          backgroundColor: AppColors.blackBackground,
          selectedItemColor: AppColors.primary,
          unselectedItemColor: AppColors.greyTextColor,
        ),
        scaffoldBackgroundColor: AppColors.blackBackground,
        fontFamily: 'SF-Pro_Display',
      ),
      initialRoute: '/',
      getPages: [
        GetPage(name: '/', page: () => Inpoint(), binding: HomePageBinding()),
        GetPage(name: '/homepage', page: () => HomePage(), binding: HomePageBinding()),
        GetPage(name: '/movie_detail', page: () => MovieDetail(), binding: MovieDetailBiding()),
        GetPage(name: '/now_playing', page: () => Movies(), binding: MoviesBinding()),
        GetPage(name: '/payment', page: () => Payment(), binding: PaymentBinding()),
        GetPage(name: '/payment', page: () => Payment(), binding: PaymentBinding()),
        GetPage(name: '/profile', page: () => Profile(), binding: ProfileBinding()),
        GetPage(name: '/select_seat', page: () => SelectSeat(), binding: SelectSeatBinding()),
        GetPage(name: '/ticket_detail', page: () => TicketDetail(), binding: TicketBinding()),
        GetPage(name: '/tickets', page: () => Tickets(), binding: TicketsBinding()),
        GetPage(name: '/confirm_otp', page: () => ConfirmOTP(), binding: ConfirmOtpBinding()),
        GetPage(name: '/enter_username', page: () => EnterUsername(), binding: EnterUsernameBinding()),
        GetPage(name: '/signup', page: () => Signup(), binding: SignupBinding()),
        GetPage(name: '/main', page: () => MainScreen(), binding: MainScreenBinding())



        // GetPage(name: '/login', page: () => Login()),
        // GetPage(name: '/signup', page: () => Signup()),
        // GetPage(name: '/confirm_otp', page: () => ConfirmOTP()),
        // GetPage(name: '/set_username', page: () => SetUsername()),
      ],
    );
  }
}