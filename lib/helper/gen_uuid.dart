class GenerateUuid {
  static String generate() {
    final currentTime = DateTime.now().millisecondsSinceEpoch;
    final random = DateTime.now().microsecondsSinceEpoch;
    return currentTime.toString() + random.toString();
  }
}