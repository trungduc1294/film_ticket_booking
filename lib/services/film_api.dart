import 'dart:convert';

import 'package:movie_booking/model/film.dart';
import 'package:http/http.dart' as http;

class FilmApi {
  static Future<List<Film>> fetchFilms() async {
    const url =
        'https://firebasestorage.googleapis.com/v0/b/movie-booking-a09fe.appspot.com/o/film.json?alt=media&token=f9b7b454-357e-4f8e-905b-7550021bc7e7';
    final uri = Uri.parse(url);

    final response = await http.get(uri);
    final body = response.body;

    final jsonList = jsonDecode(body) as List;

    final films = jsonList.map((json) => Film.fromMap(json)).toList();

    return films;
  }
}
