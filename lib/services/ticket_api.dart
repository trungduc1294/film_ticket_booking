import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:movie_booking/model/ticket.dart';

class TicketApi {
  static Future<List<Ticket>> fetchTickets() async {
    const url =
        'https://firebasestorage.googleapis.com/v0/b/movie-booking-a09fe.appspot.com/o/tickets.json?alt=media&token=0e6cd326-9077-4001-9a27-601e0704daa3';
    final uri = Uri.parse(url);

    final response = await http.get(uri);
    final body = response.body;

    final jsonList = jsonDecode(body) as List;

    final tickets = jsonList.map((json) => Ticket.fromMap(json)).toList();
    print(tickets);

    return tickets;
  }
}
