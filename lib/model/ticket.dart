class Ticket {
  final String id;
  final String filmThumbUrl;
  final String filmName;
  final String duration;
  final List<String> movieGenre;
  final String time;
  final String date;
  final String cinemaRoom;
  final List<String> seats;
  final double total;
  final String orderID;
  final String cinemaName;
  final String cinemaAddress;

  Ticket({
    required this.id,
    required this.filmThumbUrl,
    required this.filmName,
    required this.duration,
    required this.movieGenre,
    required this.time,
    required this.date,
    required this.cinemaRoom,
    required this.seats,
    required this.total,
    required this.orderID,
    required this.cinemaName,
    required this.cinemaAddress,
  });
  
  factory Ticket.fromMap(Map<String, dynamic> map) {
    return Ticket(
      id: map['id'],
      filmThumbUrl: map['film_thumb_url'],
      filmName: map['film_name'],
      duration: map['duration'],
      movieGenre: (map['movie_genre'] as String).split(', '),  // Tách chuỗi thành danh sách
      time: map['time'],
      date: map['date'],
      cinemaRoom: map['cinema_room'],
      seats: (map['seats'] as String).split(', '),  // Tách chuỗi thành danh sách
      total: double.parse(map['total']),  // Chuyển đổi từ String sang double
      orderID: map['order_id'],
      cinemaName: map['cinema_name'],
      cinemaAddress: map['cinema_address'],
    );
  }

  //to json
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'film_thumb_url': filmThumbUrl,
      'film_name': filmName,
      'duration': duration,
      'movie_genre': movieGenre.join(', '),  // Chuyển danh sách thành chuỗi
      'time': time,
      'date': date,
      'cinema_room': cinemaRoom,
      'seats': seats.join(', '),  // Chuyển danh sách thành chuỗi
      'total': total,
      'order_id': orderID,
      'cinema_name': cinemaName,
      'cinema_address': cinemaAddress,
    };
  }

  String convertMovieGenre() {
    return movieGenre.join(', ');
  }
}
