class Film {
  final String id;
  final String image;
  final String title;
  final String duration;
  final String time;
  final double rate;
  final List<String> movieGenre;
  final int censorships;
  final String desc;
  final int popular;
  final String director;
  final String directorImage;

  Film({
    required this.id,
    required this.image,
    required this.title,
    required this.duration,
    required this.time,
    required this.rate,
    required this.movieGenre,
    required this.censorships,
    required this.desc,
    required this.popular,
    required this.director,
    required this.directorImage,
  });
  
  factory Film.fromMap(Map<String, dynamic> map) {
    return Film(
      id: map['id'],
      image: map['image'],
      title: map['title'],
      duration: map['duration'],
      time: map['time'],
      rate: double.parse(map['rate']),  // Chuyển đổi từ String sang double
      movieGenre: (map['movie_genre'] as String).split(', '),  // Tách chuỗi thành danh sách
      censorships: int.parse(map['censorships']),
      desc: map['desc'],
      popular: int.parse(map['popular']),
      director: map['director'],
      directorImage: map['director_image'],
    );
  }

  //to json
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'image': image,
      'title': title,
      'duration': duration,
      'time': time,
      'rate': rate,
      'movie_genre': movieGenre.join(', '),  // Chuyển danh sách thành chuỗi
      'censorships': censorships,
      'desc': desc,
      'popular': popular,
      'director': director,
      'director_image': directorImage,
    };
  }
}
