class PaymentMethod {
  final int id;
  final String image;
  final String paymentMethod;

  
  PaymentMethod({
    required this.id,
    required this.image,
    required this.paymentMethod,
  });
}
