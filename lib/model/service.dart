class Service {
  final String image;
  final String service;

  Service({
    required this.image,
    required this.service,
  });
}
