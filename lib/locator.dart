import 'package:get_it/get_it.dart';
import 'package:movie_booking/view/screens/inpoint/inpoint_controller.dart';

final GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerLazySingleton(() => InpointController());
}